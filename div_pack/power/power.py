# power.py
__author__ = 'snippsat'
__version__ = '0.1'

def power_of(arg):
   '''Calulate Powers of exponent(^) in python'''
   return arg ** arg

if __name__ == '__main__':
   # Test,when "import power" this code will not executed
   print(power_of(4)) #--> 256
   print(power_of(8)) #--> 16777216