# company.py
import div_pack.comp_id
import div_pack.comp_name

class CompanyCheck():
    def __init__(self, name=None, _id=None):
        self.name = name
        self._id = _id

    @property
    def check_name(self):
        comp_name = div_pack.comp_name.name()
        if self.name == comp_name:
            print('Company name: <{}> is correct'.format(comp_name))
        else:
            print('Company name: <{}> is not correct'.format(comp_name))

    @property
    def check_id(self):
        comp_id = div_pack.comp_id._id()
        if self._id == comp_id:
            print('Id <{}> is correct'.format(comp_id))
        else:
             print('Id <{}> is not correct'.format(comp_id))



