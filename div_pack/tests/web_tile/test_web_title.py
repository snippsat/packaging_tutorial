import requests, pytest
from bs4 import BeautifulSoup
from find_title import web_title

def test_status():
    '''GET request to url returns a 200'''
    url = 'https://python-forum.io/'
    resp = requests.get(url)
    assert resp.status_code == 200

@pytest.mark.parametrize(
    'url, expected', [
        ('http://python-forum.io', 'Python Forum'),
        ('http://cnn.com', 'CNN - Breaking News, U.S., World, Weather, Entertainment & Video News'),
        ('http://python.org', 'Welcome to Python.org'),
    ]
)

def test_web_title(url, expected):
   '''find web site title'''
   url_get = requests.get(url)
   soup = BeautifulSoup(url_get.content, 'lxml')
   assert soup.select('head > title')[0].text == expected

