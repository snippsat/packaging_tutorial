# power.py
import pytest
__author__ = 'snippsat'
__version__ = '0.1'

@pytest.mark.parametrize(
    'arg, expected', [
        (4 , 256),
        (11, 285311670611),
        (24, 1333735776850284124449081472843776),
    ]
)

def test_power_of(arg, expected):
   '''Calulate Powers of exponent(^) in python'''
   assert arg ** arg == expected



