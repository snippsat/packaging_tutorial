# setup.py
from setuptools import setup, find_packages
from os import path

__author__ = 'snippsat'

here = path.abspath(path.dirname(__file__))
# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
   name="div_pack",
   version='0.1',
   description="Packaging tutorial",
   license='MIT',
   keywords='tuturial',
   url='https://python-forum.io/index.php',
   author_email='something@python.io',
   # find_packages() find package,exclude folder tests
   packages=find_packages(exclude=['tests']),
   include_package_data=True,
   classifiers=[
       'Intended Audience :: Education',
       'Programming Language :: Python :: 2.7',
       'Programming Language :: Python :: 3.4',
       'Programming Language :: Python :: 3.5',
       'Programming Language :: Python :: 3.6',
    ],
   # These will be installed when pip wheel run
   install_requires=[
       'beautifulsoup4',
       'requests',
       'lxml',
       'pytest',
   ],
)